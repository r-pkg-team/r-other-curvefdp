\name{curveFDP}
\alias{curveFDP}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{Fitting of a two- or three component mixture model to obtain False Discovery Proportion}
\description{
function to estimate global fdr by curve-fitting a gaussian mixture model (gmm) of two or three distributions,
one, to the left, for spectra of evident very low quality (if necessary),
one, in the center, for spectra with random hits to sequences
one, to the right, with non-random hits to sequences
the three distributions may overlap, the gmm is used to find the best seperation
assumption is that quantile of the random-hit distribution can be used to estimate how likely a score is to stem from that distribution and thus to be random.
}
\usage{
curveFDP(scores, FDPperc=5, bootstrap=1)}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{scores}{scores corresponds to the results of the scoring of sequences to spectra (vector), for each spectrum only the best score, higher scores are better}
  \item{FDPperc}{FDPperc is the desired FDP cutoff in percent (so 5 for 0.05 likelihood of having a value that extreme or higher by random)}
  \item{bootstrap}{The number of times the FDP computation should be repeated with bootstrap resampling of datasets of the same size}
}
\details{
function to estimate global fdr by curve-fitting a gaussian mixture model (gmm) of two or three distributions,
one, to the left, for spectra of evident very low quality (if necessary),
one, in the center, for spectra with random hits to sequences
one, to the right, with non-random hits to sequences
the three distributions may overlap, the gmm is used to find the best seperation
assumption is that quantile of the random-hit distribution can be used to estimate how likely a score is to stem from that distribution and thus to be random.
}
\value{
  \item{FDPcutoff}{the score atrting at which there is a likelihood of FDPperc or less that a hit is random}
  \item{aboveFDR}{number of scores above the FDPcutoff}
  \item{pvalues}{pvalues from the chi square test for appropriateness of the mixture model}
  \item{out}{mixture model as identified by mixtools package}
  \item{modelsize}{number of components in the mixture model}

}
%\references{ ~put references to the literature/web site here ~ }
\author{Bernhard Renard \email{bernhard.renard@iwr.uni-heidleberg.de}}
\examples{}
\keyword{}
