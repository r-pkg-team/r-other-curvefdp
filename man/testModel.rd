\name{testModel}
\alias{testModel}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{Testing whether a mixture model from curveFDP approximately fits the score distribution}
\description{
after binning and mild smoothing a chi-square test is used to test whether a mixture model approximately fits the socre distribution
}
\usage{
testModel(scores, out)}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{scores}{scores corresponds to the results of the scoring of sequences to spectra (vector), for each spectrum only the best score, higher scores are better}
  \item{out}{mixture model as indicated by curveFDP}
}
\details{
after binning and mild smoothing a chi-square test is used to test whether a mixture model approximately fits the socre distribution
}
\value{
  \item{p}{pvalue of the Chi square test}
}
%\references{ ~put references to the literature/web site here ~ }
\author{Bernhard Renard \email{bernhard.renard@iwr.uni-heidleberg.de}}
\examples{}
\keyword{}
