testModel<-function(scores,out)
{
###chisq test
#compute empirical distribution
#do binning
binning<-seq(range(scores)[1],range(scores)[2],(range(scores)[2]-range(scores)[1])/(length(scores)/5))
binning[length(binning)]<-max(scores)
empDistr<-hist(scores,binning,plot=FALSE)$counts
empDistr2<-c(empDistr[c(2:length(empDistr))],0)
empDistr3<-c(0,empDistr[c(1:(length(empDistr)-1))])
empDistr4<-c(empDistr[c(3:length(empDistr))],0,0)
empDistr5<-c(0,0,empDistr[c(1:(length(empDistr)-2))])
empDistr<-(empDistr+empDistr2+empDistr3+empDistr4+empDistr5)/5

theoDistr<-rep(0,length(empDistr))

if(length(out$mu)==2)
{
for(i in 1:length(theoDistr))
{theoDistr[i]<-out$lambda[1]*(pnorm(binning[i+1],out$mu[1],out$sigma[1])-pnorm(binning[i],out$mu[1],out$sigma[1]))+out$lambda[2]*(pnorm(binning[i+1],out$mu[2],out$sigma[2])-pnorm(binning[i],out$mu[2],out$sigma[2]))
}
}else{
for(i in 1:length(theoDistr))
{theoDistr[i]<-out$lambda[1]*(pnorm(binning[i+1],out$mu[1],out$sigma[1])-pnorm(binning[i],out$mu[1],out$sigma[1]))+out$lambda[2]*(pnorm(binning[i+1],out$mu[2],out$sigma[2])-pnorm(binning[i],out$mu[2],out$sigma[2]))+out$lambda[3]*(pnorm(binning[i+1],out$mu[3],out$sigma[3])-pnorm(binning[i],out$mu[3],out$sigma[3]))
}
}

theoDistr<-theoDistr/sum(theoDistr)*length(scores)

j=1
while(j<length(theoDistr))
{
if(theoDistr[j]<1)
{theoDistr[j+1]<-theoDistr[j+1]+theoDistr[j]
 empDistr[j+1]<-empDistr[j+1]+empDistr[j]
 theoDistr<-theoDistr[-j]
 empDistr<-empDistr[-j]
 binning<-binning[-(j+1)]
}
else
{j<-j+1
}
}
chistat<-sum((empDistr-theoDistr)^2/theoDistr)

p<-pchisq(chistat,length(empDistr)-9,lower.tail=FALSE)
return(p)
}
