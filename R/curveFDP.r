curveFDP<-function(scores, FDPperc=5, bootstrap=1)
{
#function to estimate global fdr by curve-fitting a gaussian mixture model of two or three distributions,
#one, to the left, for spectra of evident very low quality (if necessary),
#one, in the center, for spectra with random hits to sequences
#one, to the right, with non-random hits to sequences
#the three distributions may overlap, the gmm is used to find the best seperation
#assumption is that quantile of the random-hit distribution can be used to estimate how likely a score is to stem from that distribution and thus to be random.

#author Bernhard Y Renard, bernhard.renard@iwr.uni-heidelberg.de, 2009

#scores corresponds to the results of the scoring of sequences to spectra (vector)
#       for each spectrum only the best score, higher scores are better
#FDPperc is the desired FDR cutoff in perc (so 5 for 0.05 likelihood of having a value that extreme or higher by random)

#FDPresults contains first
# FDPcutoff, so that score at which there is a likelihood of FDPperc or less that a hit is random
# aboveFDP gives number of scores above the FDPcutoff
# pvalues, resulting from Chi-Square-test for the appropriateness of the model

require(mixtools)

aboveFDP<-vector(length=bootstrap)
FDPcutoff<-vector(length=bootstrap)
pvalues<-vector(length=bootstrap)

for(b in 1:bootstrap)
{
if(b>1)
{
scoreboot<-sample(1:length(scores),length(scores),replace=TRUE)
scoresNew<-scores[scoreboot]
}
else
{
scoresNew<-scores
}

out2<-normalmixEM(as.numeric(scoresNew), k=2)  # find mixture of 2 normals
outorder2<-order(out2$mu,decreasing=TRUE)    # order the means to identify the second largest mean -mu- among the 3 mean values

out3<-normalmixEM(as.numeric(scoresNew), k=3)  # find mixture of 3 normals
outorder3<-order(out3$mu,decreasing=TRUE)    # order the means to identify the second largest mean -mu- among the 3 mean values

if(out2$mu[outorder2[1]]<out3$mu[outorder3[2]]+ qnorm(.995,0,1)*out3$sigma[outorder3[2]])     ##if 2nd distribution of the 3comp mixture model contains center of 2nd component of 2 mixture model, then 3 mixture model is off
{out<-out2
 modelsize<-2}
else
{out<-out3
modelsize<-3}

outorder<-order(out$mu,decreasing=TRUE)    # order the means to identify the second largest mean -mu- among the 3 mean values
FDPcutoff[b]<-qnorm(1-FDPperc/100,out$mu[outorder[2]],out$sigma[outorder[2]])
aboveFDP[b]<-sum(as.numeric(scores)>=FDPcutoff)# identify how many scores are above cutoff

pvalues[b]<-testModel(scoresNew,out)

}
FDPresults<-list(FDPcutoff,aboveFDP, pvalues, out, modelsize)
names(FDPresults)=list("FDPcutoff","aboveFDP","pvalue", "out","modelsize")
return(FDPresults)
}
